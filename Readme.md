# Import App
A small application for importing local/remote xml file containing Coffee products and importing them to google sheets.appLogs are written to `var/log`.

## Installation
CD into the directory code is pulled from repository and run
`composer install`

## Settings
In order to create new spreadsheet on google sheets you will have to create new app and download credentials.json to some local folder.

## Running the app
run `bin/console app:import {source} {credentialsPath}`

Source can be:
   * **Local**: Path to file
   * **Remote**: Url to the file

## Tests
You can run tests with `bin/phpunit`


## Docker
### Application
**NOTE**: You will have to add credential files in the `/credentials` folder and import files in `/files` folder
before building the container.

CD into project root. Run:
* `docker build -t import-app .`
* `docker run -i import-app:latest bin/console app:import {source} {configFilePath}`