<?php

declare(strict_types=1);

namespace App\Tests\Shared;

use App\Read\Exception\ReadException;
use App\Read\Xml\XmlFileReader;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class FakeXmlReader implements XmlFileReader
{
    private bool $supports;
    private \SimpleXMLElement $xml;

    public function __construct(bool $supports, \SimpleXMLElement $xml)
    {
        $this->supports = $supports;
        $this->xml = $xml;
    }

    public function supports(string $source): bool
    {
        return $this->supports;
    }

    public function read(string $source): \SimpleXMLElement
    {
        return $this->xml;
    }
}
