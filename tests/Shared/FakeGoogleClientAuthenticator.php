<?php

declare(strict_types=1);

namespace App\Tests\Shared;

use App\Write\Authenticate\GoogleClientAuthenticator;
use Google_Client;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class FakeGoogleClientAuthenticator implements GoogleClientAuthenticator
{
    private bool $authenticationCalled = false;

    public function authenticate(Google_Client $client): void
    {
        $this->authenticationCalled = true;
    }

    public function isAuthenticationCalled(): bool
    {
        return $this->authenticationCalled;
    }
}
