<?php

declare(strict_types=1);

namespace App\Tests\Shared;

use App\CoffeeList;
use App\Write\ListToArray;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class FakeListToArray implements ListToArray
{
    private array $returnArray = [];

    public function willReturnArray(array $array): void
    {
        $this->returnArray = $array;
    }
    public function createArrayWithHeader(CoffeeList $coffeeList): array
    {
        return $this->returnArray;
    }
}
