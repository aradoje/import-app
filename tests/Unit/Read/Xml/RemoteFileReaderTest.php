<?php

declare(strict_types=1);

namespace App\Tests\Unit\Read\Xml;

use App\Read\Xml\LocalXmlFileReader;
use App\Read\Xml\RemoteXmlFileReader;
use PHPUnit\Framework\TestCase;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class RemoteFileReaderTest extends TestCase
{
    private RemoteXmlFileReader $reader;

    protected function setUp(): void
    {
        parent::setUp();
        $this->reader = new RemoteXmlFileReader();
    }

    /**
     * @dataProvider getSupports
     */
    public function testSupports(string $source, bool $supports): void
    {
        self::assertEquals($supports, $this->reader->supports($source));
    }

    public function getSupports(): iterable
    {
        yield ['local:/usr/some-file.xml', false];
        yield ['local:/some-file.xml', false];
        yield ['local:some-file.xml', false];
        yield ['https://domain.host', true];
        yield ['http://domain.host', true];
    }
}
