<?php

declare(strict_types=1);

namespace App\Tests\Unit\Read\Xml;

use App\Read\Xml\LocalXmlFileReader;
use PHPUnit\Framework\TestCase;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class LocalFileReaderTest extends TestCase
{
    private LocalXmlFileReader $reader;

    protected function setUp(): void
    {
        parent::setUp();
        $this->reader = new LocalXmlFileReader();
    }

    /**
     * @dataProvider getSupports
     */
    public function testSupports(string $source, bool $supports): void
    {
        self::assertEquals($supports, $this->reader->supports($source), sprintf('Should return %b for path `%s`', $supports, $source));
    }

    public function getSupports(): iterable
    {
        yield [__DIR__ .'/files/xml-file.xml', true];
        yield [__DIR__ .'/files/import-data.xml', true];
        yield [__DIR__ . 'files/non-xml-file.docx', false];
        yield ['https://domain.host/some-xml.xml', false];
        yield ['http://domain.host/path/some-xml.xml', false];
    }

    public function testRead(): void
    {
        $source = __DIR__ . '/files/import-data.xml';

        $xml = $this->reader->read($source);
        self::assertGreaterThan(0, $xml->count());
    }
}
