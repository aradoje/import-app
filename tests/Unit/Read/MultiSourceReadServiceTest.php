<?php

declare(strict_types=1);

namespace App\Tests\Unit\Read;

use App\Read\Exception\SourceNotSupported;
use App\Read\MultiSourceReadService;
use App\Tests\Shared\FakeXmlReader;
use PHPUnit\Framework\TestCase;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class MultiSourceReadServiceTest extends TestCase
{
    public function testItThrowsExceptionIfNoXmlReaderSupportsSource(): void
    {
        $reader = new MultiSourceReadService([new FakeXmlReader(false, new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><catalog><item></item></catalog>'))]);
        $this->expectException(SourceNotSupported::class);

        $reader->readFromSource('some-source');
    }

    public function testItCreatesWithAllNonBooleanData(): void
    {
        $reader = new MultiSourceReadService([
            new FakeXmlReader(true, new \SimpleXMLElement(
                <<<XML
                <?xml version="1.0" encoding="utf-8"?>
                <catalog>
                    <item>
                        <entity_id>340</entity_id>
                        <CategoryName><![CDATA[Green Mountain Ground Coffee]]></CategoryName>
                        <sku>20</sku>
                        <name><![CDATA[Green Mountain Coffee French Roast Ground Coffee 24 2.2oz Bag]]></name>
                        <description></description>
                        <shortdesc><![CDATA[Green Mountain Coffee French Roast Ground Coffee 24 2.2oz Bag steeps cup after cup of smoky-sweet, complex dark roast coffee from Green Mountain Ground Coffee.]]></shortdesc>
                        <price>41.6000</price>
                        <link>http://www.coffeeforless.com/green-mountain-coffee-french-roast-ground-coffee-24-2-2oz-bag.html</link>
                        <image>http://mcdn.coffeeforless.com/media/catalog/product/images/uploads/intro/frac_box.jpg</image>
                        <Brand><![CDATA[Green Mountain Coffee]]></Brand>
                        <Rating>1</Rating>
                        <CaffeineType>Caffeinated</CaffeineType>
                        <Count>24</Count>
                        <Flavored>Yes</Flavored>
                        <Seasonal>Yes</Seasonal>
                        <Instock>Yes</Instock>
                        <Facebook>1</Facebook>
                        <IsKCup>1</IsKCup>
                    </item>
                </catalog>
                XML
            ))
        ]);

        $coffeeList = $reader->readFromSource('some-source');
        self::assertCount(1, $coffeeList->getItems());
        $coffee = $coffeeList->getItems()[0];
        self::assertEquals(340, $coffee->getEntityId());
        self::assertEquals('Green Mountain Ground Coffee', $coffee->getCategoryName());
        self::assertEquals('20', $coffee->getSku());
        self::assertEquals('Green Mountain Coffee French Roast Ground Coffee 24 2.2oz Bag', $coffee->getName());
        self::assertEquals('', $coffee->getDescription());
        self::assertEquals('Green Mountain Coffee French Roast Ground Coffee 24 2.2oz Bag steeps cup after cup of smoky-sweet, complex dark roast coffee from Green Mountain Ground Coffee.', $coffee->getShortDescription());
        self::assertEquals('41.6000', $coffee->getPrice());
        self::assertEquals('http://www.coffeeforless.com/green-mountain-coffee-french-roast-ground-coffee-24-2-2oz-bag.html', $coffee->getLink());
        self::assertEquals('http://mcdn.coffeeforless.com/media/catalog/product/images/uploads/intro/frac_box.jpg', $coffee->getImage());
        self::assertEquals('Green Mountain Coffee', $coffee->getBrand());
        self::assertEquals('Caffeinated', $coffee->getCaffeineType());
        self::assertEquals(24, $coffee->getCount());
    }

    public function testItCreatesWithRequiredStringData(): void
    {
        $reader = new MultiSourceReadService([
            new FakeXmlReader(true, new \SimpleXMLElement(
                <<<XML
                <?xml version="1.0" encoding="utf-8"?>
                <catalog>
                    <item>
                        <entity_id>340</entity_id>
                        <CategoryName><![CDATA[]]></CategoryName>
                        <sku>20</sku>
                        <name><![CDATA[]]></name>
                        <description></description>
                        <shortdesc><![CDATA[]]></shortdesc>
                        <price>41.6000</price>
                        <link>http://www.coffeeforless.com/green-mountain-coffee-french-roast-ground-coffee-24-2-2oz-bag.html</link>
                        <image>http://mcdn.coffeeforless.com/media/catalog/product/images/uploads/intro/frac_box.jpg</image>
                        <Brand><![CDATA[]]></Brand>
                        <Rating>1</Rating>
                        <CaffeineType></CaffeineType>
                        <Count>24</Count>
                        <Flavored>Yes</Flavored>
                        <Seasonal>Yes</Seasonal>
                        <Instock>Yes</Instock>
                        <Facebook>1</Facebook>
                        <IsKCup>1</IsKCup>
                    </item>
                </catalog>
                XML
            ))
        ]);

        $coffeeList = $reader->readFromSource('some-source');
        self::assertCount(1, $coffeeList->getItems());
        $coffee = $coffeeList->getItems()[0];
        self::assertEquals('', $coffee->getCategoryName());
        self::assertEquals('', $coffee->getName());
        self::assertEquals('', $coffee->getDescription());
        self::assertEquals('', $coffee->getShortDescription());
        self::assertEquals('', $coffee->getBrand());
        self::assertEquals('', $coffee->getCaffeineType());
    }

    public function testItCreatesWithBooleanDataTrue(): void
    {
        $reader = new MultiSourceReadService([
            new FakeXmlReader(true, new \SimpleXMLElement(
                <<<XML
                <?xml version="1.0" encoding="utf-8"?>
                <catalog>
                    <item>
                        <entity_id>340</entity_id>
                        <CategoryName><![CDATA[xx]]></CategoryName>
                        <sku>20</sku>
                        <name><![CDATA[xx]]></name>
                        <description><![CDATA[xx]]></description>
                        <shortdesc><![CDATA[xx]]></shortdesc>
                        <price>41.6000</price>
                        <link>http://www.coffeeforless.com/green-mountain-coffee-french-roast-ground-coffee-24-2-2oz-bag.html</link>
                        <image>http://mcdn.coffeeforless.com/media/catalog/product/images/uploads/intro/frac_box.jpg</image>
                        <Brand><![CDATA[xx]]></Brand>
                        <Rating>1</Rating>
                        <CaffeineType>xx</CaffeineType>
                        <Count>24</Count>
                        <Flavored>Yes</Flavored>
                        <Seasonal>Yes</Seasonal>
                        <Instock>Yes</Instock>
                        <Facebook>1</Facebook>
                        <IsKCup>1</IsKCup>
                    </item>
                </catalog>
                XML
            ))
        ]);

        $coffeeList = $reader->readFromSource('some-source');
        self::assertCount(1, $coffeeList->getItems());
        $coffee = $coffeeList->getItems()[0];
        self::assertTrue($coffee->getFlavored());
        self::assertTrue($coffee->getSeasonal());
        self::assertTrue($coffee->isInStock());
        self::assertTrue($coffee->isFacebook());
        self::assertTrue($coffee->isKCup());
    }

    public function testItCreatesWithBooleanDataFalse(): void
    {
        $reader = new MultiSourceReadService([
            new FakeXmlReader(true, new \SimpleXMLElement(
                <<<XML
                <?xml version="1.0" encoding="utf-8"?>
                <catalog>
                    <item>
                        <entity_id>340</entity_id>
                        <CategoryName><![CDATA[xx]]></CategoryName>
                        <sku>20</sku>
                        <name><![CDATA[xx]]></name>
                        <description><![CDATA[xx]]></description>
                        <shortdesc><![CDATA[xx]]></shortdesc>
                        <price>41.6000</price>
                        <link>http://www.coffeeforless.com/green-mountain-coffee-french-roast-ground-coffee-24-2-2oz-bag.html</link>
                        <image>http://mcdn.coffeeforless.com/media/catalog/product/images/uploads/intro/frac_box.jpg</image>
                        <Brand><![CDATA[xx]]></Brand>
                        <Rating>1</Rating>
                        <CaffeineType>xx</CaffeineType>
                        <Count>24</Count>
                        <Flavored>No</Flavored>
                        <Seasonal>No</Seasonal>
                        <Instock>No</Instock>
                        <Facebook>0</Facebook>
                        <IsKCup>0</IsKCup>
                    </item>
                </catalog>
                XML
            ))
        ]);

        $coffeeList = $reader->readFromSource('some-source');
        self::assertCount(1, $coffeeList->getItems());
        $coffee = $coffeeList->getItems()[0];
        self::assertFalse($coffee->getFlavored());
        self::assertFalse($coffee->getSeasonal());
        self::assertFalse($coffee->isInStock());
        self::assertFalse($coffee->isFacebook());
        self::assertFalse($coffee->isKCup());
    }

    public function testItCreatesWithBooleanDataUndefined(): void
    {
        $reader = new MultiSourceReadService([
            new FakeXmlReader(true, new \SimpleXMLElement(
                <<<XML
                <?xml version="1.0" encoding="utf-8"?>
                <catalog>
                    <item>
                        <entity_id>340</entity_id>
                        <CategoryName><![CDATA[xx]]></CategoryName>
                        <sku>20</sku>
                        <name><![CDATA[xx]]></name>
                        <description><![CDATA[xx]]></description>
                        <shortdesc><![CDATA[xx]]></shortdesc>
                        <price>41.6000</price>
                        <link>http://www.coffeeforless.com/green-mountain-coffee-french-roast-ground-coffee-24-2-2oz-bag.html</link>
                        <image>http://mcdn.coffeeforless.com/media/catalog/product/images/uploads/intro/frac_box.jpg</image>
                        <Brand><![CDATA[xx]]></Brand>
                        <Rating>1</Rating>
                        <CaffeineType>xx</CaffeineType>
                        <Count>24</Count>
                        <Flavored></Flavored>
                        <Seasonal></Seasonal>
                        <Instock></Instock>
                        <Facebook></Facebook>
                        <IsKCup></IsKCup>
                    </item>
                </catalog>
                XML
            ))
        ]);

        $coffeeList = $reader->readFromSource('some-source');
        self::assertCount(1, $coffeeList->getItems());
        $coffee = $coffeeList->getItems()[0];
        self::assertNull($coffee->getFlavored());
        self::assertNull($coffee->getSeasonal());
        self::assertFalse($coffee->isInStock());
        self::assertFalse($coffee->isFacebook());
        self::assertFalse($coffee->isKCup());
    }
}
