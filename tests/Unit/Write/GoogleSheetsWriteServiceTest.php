<?php

declare(strict_types=1);

namespace App\Tests\Unit\Write;

use App\CoffeeList;
use App\Tests\Shared\FakeGoogleClientAuthenticator;
use App\Tests\Shared\FakeListToArray;
use App\Write\GoogleSheetsWriteService;
use Google_Service_Sheets;
use Google_Service_Sheets_Resource_Spreadsheets;
use Google_Service_Sheets_Resource_SpreadsheetsValues;
use Google_Service_Sheets_Spreadsheet;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class GoogleSheetsWriteServiceTest extends TestCase
{
    private FakeListToArray $listToArray;
    private FakeGoogleClientAuthenticator $googleClientAuthenticator;
    private GoogleSheetsWriteService $writeService;
    private MockObject $googleClient;
    private MockObject $sheetsService;
    private MockObject $spreadsheets;
    private MockObject $spreadsheetValues;

    protected function setUp(): void
    {
        parent::setUp();
        $this->googleClient = $this->getMockBuilder(\Google_Client::class)->disableOriginalConstructor()->getMock();
        $this->googleClient->method('getLogger')->willReturn(
            $this->getMockBuilder(LoggerInterface::class)->getMock()
        );

        $this->sheetsService = $this->getMockBuilder(\Google_Service_Sheets::class)->disableOriginalConstructor()->getMock();
        $this->sheetsService->method('getClient')->willReturn($this->googleClient);

        $this->spreadsheets = $this->getMockBuilder(Google_Service_Sheets_Resource_Spreadsheets::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->spreadsheets->method('create')
            ->willReturn(new Google_Service_Sheets_Spreadsheet());
        $this->sheetsService->spreadsheets = $this->spreadsheets;

        $this->spreadsheetValues =  $this->getMockBuilder(Google_Service_Sheets_Resource_SpreadsheetsValues::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->sheetsService->spreadsheets_values = $this->spreadsheetValues;

        $this->writeService = new GoogleSheetsWriteService(
            $this->listToArray = new FakeListToArray(),
            $this->googleClientAuthenticator = new FakeGoogleClientAuthenticator(),
            $this->sheetsService
        );
    }

    public function testWriteConfiguresClient(): void
    {
        $this->googleClient->expects(self::once())
            ->method('setApplicationName')
            ->with('ImportApp');

        $this->googleClient->expects(self::once())
            ->method('setScopes')
            ->with(Google_Service_Sheets::SPREADSHEETS);

        $this->googleClient->expects(self::once())
            ->method('setAuthConfig')
            ->with('/credentials.json');

        $this->googleClient->expects(self::once())
            ->method('setAccessType')
            ->with('offline');

        $this->googleClient->expects(self::once())
            ->method('setPrompt')
            ->with('select_account consent');

        $this->writeService->write(new CoffeeList(), '/credentials.json');
    }

    public function testWriteAuthenticatesClient(): void
    {
        $this->writeService->write(new CoffeeList(), 'default');
        self::assertTrue($this->googleClientAuthenticator->isAuthenticationCalled());
    }

    public function testWriteCreatesNewSpreadsheet(): void
    {
        $this->spreadsheets->expects(self::once())->method('create');
        $this->writeService->write(new CoffeeList(), 'default');
        self::assertTrue($this->googleClientAuthenticator->isAuthenticationCalled());
    }

    public function testWriteUpdatesNewSpreadsheetWithData(): void
    {
        $this->spreadsheetValues->expects(self::once())->method('update');
        $this->writeService->write(new CoffeeList(), 'default');
        self::assertTrue($this->googleClientAuthenticator->isAuthenticationCalled());
    }
}
