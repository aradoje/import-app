<?php

declare(strict_types=1);

namespace App\Tests\Unit\Write;

use App\Coffee;
use App\CoffeeList;
use App\Write\CoffeeListToArray;
use PHPUnit\Framework\TestCase;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class CoffeeListToArrayTest extends TestCase
{
    private CoffeeListToArray $listToArray;

    protected function setUp(): void
    {
        parent::setUp();
        $this->listToArray = new CoffeeListToArray();
    }

    public function testCreateArrayWithHeaderAddsProperHeader(): void
    {
        $coffeeList = new CoffeeList();
        $array = $this->listToArray->createArrayWithHeader($coffeeList);

        self::assertCount(1, $array);

        $header = $array[0];
        self::assertCount(18, $header);

        self::assertEquals('EntityId', $header[0]);
        self::assertEquals('CategoryName', $header[1]);
        self::assertEquals('Sku', $header[2]);
        self::assertEquals('Name', $header[3]);
        self::assertEquals('Description', $header[4]);
        self::assertEquals('ShortDescription', $header[5]);
        self::assertEquals('Price', $header[6]);
        self::assertEquals('Link', $header[7]);
        self::assertEquals('Image', $header[8]);
        self::assertEquals('Brand', $header[9]);
        self::assertEquals('Rating', $header[10]);
        self::assertEquals('CaffeineType', $header[11]);
        self::assertEquals('Count', $header[12]);
        self::assertEquals('Flavored', $header[13]);
        self::assertEquals('Seasonal', $header[14]);
        self::assertEquals('InStock', $header[15]);
        self::assertEquals('Facebook', $header[16]);
        self::assertEquals('KCup', $header[17]);
    }

    public function testCreateArrayWithHeaderWithCoffeeBoolAllTrue(): void
    {
        $coffeeList = new CoffeeList();

        $coffee = new Coffee(1);
        $coffee->setCategoryName('CategoryName');
        $coffee->setSku('Sku');
        $coffee->setName('Name');
        $coffee->setDescription('Description');
        $coffee->setShortDescription('ShortDescription');
        $coffee->setPrice('100.00');
        $coffee->setLink('http://link.link');
        $coffee->setImage('http://image.image');
        $coffee->setBrand('Brand');
        $coffee->setRating(1);
        $coffee->setCaffeineType('Caffeinated');
        $coffee->setCount(10);
        $coffee->setFlavored(true);
        $coffee->setSeasonal(true);
        $coffee->setInStock(true);
        $coffee->setFacebook(true);
        $coffee->setKCup(true);

        $coffeeList->addItem($coffee);

        $array = $this->listToArray->createArrayWithHeader($coffeeList);

        self::assertCount(2, $array);

        $data = $array[1];
        self::assertCount(18, $data);

        self::assertEquals(1, $data[0]);
        self::assertEquals('CategoryName', $data[1]);
        self::assertEquals('Sku', $data[2]);
        self::assertEquals('Name', $data[3]);
        self::assertEquals('Description', $data[4]);
        self::assertEquals('ShortDescription', $data[5]);
        self::assertEquals('100.00', $data[6]);
        self::assertEquals('http://link.link', $data[7]);
        self::assertEquals('http://image.image', $data[8]);
        self::assertEquals('Brand', $data[9]);
        self::assertEquals(1, $data[10]);
        self::assertEquals('Caffeinated', $data[11]);
        self::assertEquals(10, $data[12]);
        self::assertEquals('Yes', $data[13]);
        self::assertEquals('Yes', $data[14]);
        self::assertEquals('Yes', $data[15]);
        self::assertEquals('Yes', $data[16]);
        self::assertEquals('Yes', $data[17]);
    }

    public function testCreateArrayWithHeaderWithCoffeeBoolAllFalse(): void
    {
        $coffeeList = new CoffeeList();

        $coffee = new Coffee(1);
        $coffee->setCategoryName('CategoryName');
        $coffee->setSku('Sku');
        $coffee->setName('Name');
        $coffee->setDescription('Description');
        $coffee->setShortDescription('ShortDescription');
        $coffee->setPrice('100.00');
        $coffee->setLink('http://link.link');
        $coffee->setImage('http://image.image');
        $coffee->setBrand('Brand');
        $coffee->setRating(1);
        $coffee->setCaffeineType('Caffeinated');
        $coffee->setCount(10);
        $coffee->setFlavored(false);
        $coffee->setSeasonal(false);
        $coffee->setInStock(false);
        $coffee->setFacebook(false);
        $coffee->setKCup(false);

        $coffeeList->addItem($coffee);

        $array = $this->listToArray->createArrayWithHeader($coffeeList);

        self::assertCount(2, $array);

        $data = $array[1];
        self::assertCount(18, $data);

        self::assertEquals('No', $data[13]);
        self::assertEquals('No', $data[14]);
        self::assertEquals('No', $data[15]);
        self::assertEquals('No', $data[16]);
        self::assertEquals('No', $data[17]);
    }

    public function testCreateArrayWithHeaderWithCoffeeOptionalBoolsNull(): void
    {
        $coffeeList = new CoffeeList();

        $coffee = new Coffee(1);
        $coffee->setCategoryName('xx');
        $coffee->setSku('xx');
        $coffee->setName('xx');
        $coffee->setDescription('xx');
        $coffee->setShortDescription('xx');
        $coffee->setPrice('100.00');
        $coffee->setLink('http://link.link');
        $coffee->setImage('http://image.image');
        $coffee->setBrand('xx');
        $coffee->setRating(1);
        $coffee->setCaffeineType('xx');
        $coffee->setCount(10);
        $coffee->setInStock(true);
        $coffee->setFacebook(true);
        $coffee->setKCup(true);

        $coffeeList->addItem($coffee);

        $array = $this->listToArray->createArrayWithHeader($coffeeList);

        self::assertCount(2, $array);

        $data = $array[1];
        self::assertCount(18, $data);

        self::assertEquals('', $data[13]);
        self::assertEquals('', $data[14]);
    }
}
