FROM php:7.4-cli-alpine

ENV COMPOSER_ALLOW_SUPERUSER 1

WORKDIR /srv/app

COPY composer.json composer.lock symfony.lock ./
COPY bin bin/
COPY .env .env.test phpunit.xml.dist ./
COPY files files/
COPY credentials credentials/
COPY config config/
COPY public public/
COPY src src/

RUN echo "memory_limit=1024M" > /usr/local/etc/php/conf.d/memory-limit.ini
RUN curl --silent --show-error https://getcomposer.org/installer | php && \
    php composer.phar install --prefer-dist --no-progress --no-suggest --optimize-autoloader --classmap-authoritative  --no-interaction && \
    php composer.phar clear-cache && \
    rm -rf /usr/src/php

VOLUME /srv/app/var
