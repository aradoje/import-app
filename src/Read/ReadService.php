<?php

declare(strict_types=1);

namespace App\Read;

use App\CoffeeList;
use App\Read\Exception\ReadException;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
interface ReadService
{
    /**
     * @throws ReadException
     */
    public function readFromSource(string $source): CoffeeList;
}
