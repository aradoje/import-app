<?php

declare(strict_types=1);

namespace App\Read\Xml;

use App\Read\Exception\ReadFailed;
use App\Read\Exception\SourceNotSupported;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class RemoteXmlFileReader implements XmlFileReader
{
    public function supports(string $source): bool
    {
        return 0 === strpos($source, 'http:')
            || 0 === strpos($source, 'https:');
    }

    /**
     * @throws ReadFailed
     */
    public function read(string $source): \SimpleXMLElement
    {
        if (false === $this->supports($source)) {
            throw SourceNotSupported::create($source);
        }

        $content = $this->getFileContent($source);

        return $this->getXml($content);
    }

    /**
     * @throws ReadFailed
     */
    private function getFileContent(string $path): string
    {
        try {
            $content = file_get_contents($path);
        } catch (\Exception $e) {
            throw ReadFailed::withPrevious($e);
        }

        if (false === $content) {
            throw ReadFailed::withMessage('Failed to get content from file.');
        }

        return $content;
    }

    /**
     * @throws ReadFailed
     */
    private function getXml(string $content): \SimpleXMLElement
    {
        try {
            $xml = simplexml_load_string($content, null, LIBXML_NOCDATA);
        } catch (\Exception $e) {
            throw ReadFailed::withPrevious($e);
        }

        if (false === $xml) {
            throw ReadFailed::withMessage('Failed to get content from file.');
        }

        return $xml;
    }
}
