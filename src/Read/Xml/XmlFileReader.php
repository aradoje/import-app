<?php

declare(strict_types=1);

namespace App\Read\Xml;

use App\Read\Exception\ReadException;

;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
interface XmlFileReader
{
    public function supports(string $source): bool;

    /**
     * @throws ReadException
     */
    public function read(string $source): \SimpleXMLElement;
}
