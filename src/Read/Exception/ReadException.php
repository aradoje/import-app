<?php

declare(strict_types=1);

namespace App\Read\Exception;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
interface ReadException extends \Throwable
{
}
