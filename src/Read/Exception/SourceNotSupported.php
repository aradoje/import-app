<?php

declare(strict_types=1);

namespace App\Read\Exception;

use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class SourceNotSupported extends Exception implements ReadException
{
    public static function create(string $source): self
    {
        return new self(sprintf(
            'Source "%s" is not supported',
            $source
        ));
    }
}
