<?php

declare(strict_types=1);

namespace App\Read\Exception;

use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class ReadFailed extends Exception implements ReadException
{
    public static function withMessage(string $message): self
    {
        return new self(sprintf(
            'Could not read from file: %s',
            $message
        ));
    }

    public static function withPrevious(\Throwable $previous): self
    {
        return new self(
            sprintf(
                'Could not read from file: %s',
                $previous->getMessage(),
            ),
            0,
            $previous
        );
    }
}
