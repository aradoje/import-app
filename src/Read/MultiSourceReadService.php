<?php

declare(strict_types=1);

namespace App\Read;

use App\Coffee;
use App\CoffeeList;
use App\Read\Exception\SourceNotSupported;
use App\Read\Xml\XmlFileReader;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class MultiSourceReadService implements ReadService
{
    /**
     * @var XmlFileReader[]
     */
    private iterable $fileReaders;

    public function __construct(iterable $fileReaders)
    {
        $this->fileReaders = $fileReaders;
    }

    /**
     * @throws Exception\ReadException
     */
    public function readFromSource(string $source): CoffeeList
    {
        $xml = $this->readXml($source);

        $list = new CoffeeList();
        foreach ($xml as $item) {
            $coffee = $this->createCoffeeFromItem($item);
            $list->addItem($coffee);
        }

        return $list;
    }

    /**
     * @throws Exception\ReadException
     */
    private function readXml(string $source): \SimpleXMLElement
    {
        foreach ($this->fileReaders as $fileReader) {
            if (!$fileReader->supports($source)) {
                continue;
            }

            return $fileReader->read($source);
        }

        throw SourceNotSupported::create($source);
    }

    private function createCoffeeFromItem(\SimpleXMLElement $item): Coffee
    {
        $entity = new Coffee((int) $item->entity_id);

        $entity->setCategoryName((string) $item->CategoryName);
        $entity->setSku((string) $item->sku);
        $entity->setName((string) $item->name);
        $entity->setDescription((string) $item->description);
        $entity->setShortDescription((string) $item->shortdesc);
        $entity->setPrice((string) $item->price);
        $entity->setLink((string) $item->link);
        $entity->setImage((string) $item->image);
        $entity->setBrand((string) $item->Brand);
        $entity->setRating((int) $item->Rating);
        $entity->setCaffeineType((string) $item->CaffeineType);
        $entity->setCount((int) $item->Count);

        if ('Yes' === (string)$item->Flavored) {
            $entity->setFlavored(true);
        } elseif ('No' === (string)$item->Flavored) {
            $entity->setFlavored(false);
        }

        if ('Yes' === (string)$item->Seasonal) {
            $entity->setSeasonal(true);
        } elseif ('No' === (string)$item->Seasonal) {
            $entity->setSeasonal(false);
        }

        $entity->setInStock('Yes' === (string)$item->Instock);
        $entity->setFacebook('1' === (string)$item->Facebook);

        $entity->setKCup('1' === (string)$item->IsKCup);

        return $entity;
    }
}
