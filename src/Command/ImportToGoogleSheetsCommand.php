<?php

declare(strict_types=1);

namespace App\Command;

use App\Read\ReadService;
use App\Write\GoogleSheetsWriteService;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class ImportToGoogleSheetsCommand extends Command
{
    protected static $defaultName = 'app:import';
    private ReadService $readService;
    private GoogleSheetsWriteService $writeService;
    private LoggerInterface $logger;

    public function __construct(
        ReadService $readService,
        GoogleSheetsWriteService $writeService,
        LoggerInterface $logger
    ) {
        parent::__construct();
        $this->readService = $readService;
        $this->logger = $logger;
        $this->writeService = $writeService;
    }

    protected function configure()
    {
        $this
            ->addArgument(
                'source',
                InputArgument::REQUIRED,
                'Source to import file from. It can be in format 
                "local:{path}" for local file or 
                "https://host.domain/uri" for remote file.'
            )
            ->addArgument(
                'credentialsFilePath',
                InputArgument::REQUIRED,
                'Configuration json file with Google app credentials'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $source = (string) $input->getArgument('source');
        $credentialsFilePath = (string) $input->getArgument('credentialsFilePath');

        try {
            $productList = $this->readService->readFromSource($source);
            $this->writeService->write($productList, $credentialsFilePath);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [
                'exception' => $e
            ]);

            $io->error('Failed to read from source');

            return 1;
        }

        $io->success('Imported!');

        return 0;
    }
}
