<?php

declare(strict_types=1);

namespace App\Write\Exception;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class WriteFailed extends \Exception implements WriteException
{
    public static function withPrevious(\Exception $exception): self
    {
        return new self(
            sprintf(
                'Write failed due to error: %s',
                $exception->getMessage(),
            ),
            0,
            $exception
        );
    }

    public static function withJsonException(\JsonException $exception): self
    {
        return new self(
            sprintf(
                'Write failed due to Json error: %s',
                $exception->getMessage(),
            ),
            0,
            $exception
        );
    }

    public static function withGoogleException(\Google_Exception $exception): self
    {
        return new self(
            sprintf(
                'Write failed due to Google error: %s',
                $exception->getMessage(),
            ),
            0,
            $exception
        );
    }

    public static function withTokenError(string $message): self
    {
        return new self(sprintf(
            'Write failed due to token error: %s',
            $message
        ));
    }

    public static function couldNotCreateDirectory(string $path): self
    {
        return new self(sprintf(
            'Write failed: Could not create directory "%s"',
            $path
        ));
    }
}
