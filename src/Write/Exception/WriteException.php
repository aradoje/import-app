<?php

declare(strict_types=1);

namespace App\Write\Exception;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
interface WriteException
{
}
