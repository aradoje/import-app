<?php

declare(strict_types=1);

namespace App\Write\Authenticate;

use App\Write\Exception\WriteFailed;
use Google_Client;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class DefaultGoogleClientAuthenticator implements GoogleClientAuthenticator
{
    private string $tokenPath;

    public function __construct(
        string $tokenPath
    ) {
        $this->tokenPath = $tokenPath;
    }

    /**
     * @throws WriteFailed
     */
    public function authenticate(Google_Client $client): void
    {
        // The file token.json stores the user's access and refresh tokens, and is
        // created automatically when the authorization flow completes for the first
        // time.
        $tokenPath = sprintf('%s/token.json', $this->tokenPath);
        if (file_exists($tokenPath)) {
            $this->setExistingAccessToken($tokenPath, $client);
        }

        // If there is no previous token or it's expired.
        if (!$client->isAccessTokenExpired()) {
            return;
        }

        // Refresh the token if possible, else fetch a new one.
        if ($client->getRefreshToken()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        } else {
            $this->fetchNewAccessToken($client);
        }
        $this->saveToken($tokenPath, $client->getAccessToken());
    }


    /**
     * @throws WriteFailed
     */
    private function setExistingAccessToken(string $tokenPath, Google_Client $client): void
    {
        try {
            $accessToken = json_decode(file_get_contents($tokenPath), true, 512, JSON_THROW_ON_ERROR);
        } catch (\JsonException $e) {
            throw WriteFailed::withJsonException($e);
        }
        $client->setAccessToken($accessToken);
    }

    /**
     * @param Google_Client $client
     *
     * @throws WriteFailed
     */
    private function fetchNewAccessToken(Google_Client $client): void
    {
        // Request authorization from the user.
        $authUrl = $client->createAuthUrl();
        printf("Open the following link in your browser:\n%s\n", $authUrl);
        print 'Enter verification code: ';
        $authCode = trim(fgets(STDIN));

        // Exchange authorization code for an access token.
        $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
        $client->setAccessToken($accessToken);

        // Check to see if there was an error.
        if (array_key_exists('error', $accessToken)) {
            throw WriteFailed::withTokenError(implode(', ', $accessToken));
        }
    }

    /**
     * @throws WriteFailed
     */
    private function saveToken(string $tokenPath, array $accessToken): void
    {
        if (!file_exists(dirname($tokenPath))
            && !mkdir($concurrentDirectory = dirname($tokenPath), 0700, true)
            && !is_dir($concurrentDirectory)
        ) {
            throw WriteFailed::couldNotCreateDirectory($concurrentDirectory);
        }

        try {
            file_put_contents($tokenPath, json_encode($accessToken, JSON_THROW_ON_ERROR));
        } catch (\JsonException $e) {
            throw WriteFailed::withJsonException($e);
        }
    }
}
