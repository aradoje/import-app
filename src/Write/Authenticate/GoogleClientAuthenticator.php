<?php

declare(strict_types=1);

namespace App\Write\Authenticate;

use App\Write\Exception\WriteFailed;
use Google_Client;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
interface GoogleClientAuthenticator
{
    /**
     * @throws WriteFailed
     */
    public function authenticate(Google_Client $client): void;
}
