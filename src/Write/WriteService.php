<?php

declare(strict_types=1);

namespace App\Write;

use App\CoffeeList;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
interface WriteService
{
    public function write(CoffeeList $coffeeList, string $listName);
}
