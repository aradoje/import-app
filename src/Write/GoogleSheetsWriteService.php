<?php

declare(strict_types=1);

namespace App\Write;

use App\CoffeeList;
use App\Write\Authenticate\GoogleClientAuthenticator;
use App\Write\Exception\WriteFailed;
use Google_Service_Sheets;
use Google_Service_Sheets_Spreadsheet;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class GoogleSheetsWriteService implements WriteService
{
    private ListToArray $listToArray;
    private GoogleClientAuthenticator $authenticator;
    private Google_Service_Sheets $sheetsService;

    public function __construct(
        ListToArray $listToArray,
        GoogleClientAuthenticator $authenticator,
        Google_Service_Sheets $sheetsService
    ) {
        $this->listToArray = $listToArray;
        $this->authenticator = $authenticator;
        $this->sheetsService = $sheetsService;
    }

    /**
     * @throws WriteFailed
     */
    public function write(CoffeeList $coffeeList, string $credentialsFilePath): void
    {
        $this->configureAndAuthenticateClient($credentialsFilePath);

        $spreadsheet = new Google_Service_Sheets_Spreadsheet([
            'properties' => [
                'title' => 'ImportApp'
            ]
        ]);

        $values = $this->listToArray->createArrayWithHeader($coffeeList);
        $range = sprintf('Sheet1!A1:R%d', count($values));
        $spreadsheetValueRange = new \Google_Service_Sheets_ValueRange([
            'values' => $values,
        ]);

        try {
            $spreadsheet = $this->sheetsService->spreadsheets->create($spreadsheet, [
                'fields' => 'spreadsheetId'
            ]);
            $this->sheetsService->spreadsheets_values->update($spreadsheet->spreadsheetId, $range, $spreadsheetValueRange, ['valueInputOption' => 'RAW']);
        } catch (\Exception $e) {
            throw WriteFailed::withPrevious($e);
        }
    }

    /**
     * @throws WriteFailed
     */
    private function configureAndAuthenticateClient(string $credentialsFilePath): void
    {
        $client = $this->sheetsService->getClient();
        $client->setApplicationName('ImportApp');
        $client->setScopes(Google_Service_Sheets::SPREADSHEETS);

        try {
            $client->setAuthConfig($credentialsFilePath);
        } catch (\Google_Exception $e) {
            throw WriteFailed::withGoogleException($e);
        }

        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');
        $this->authenticator->authenticate($client);
    }
}
