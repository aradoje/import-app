<?php

declare(strict_types=1);

namespace App\Write;

use App\Coffee;
use App\CoffeeList;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class CoffeeListToArray implements ListToArray
{
    public function createArrayWithHeader(CoffeeList $coffeeList): array
    {
        $values = [];
        $values[] = $this->createHeader();

        foreach ($coffeeList->getItems() as $item) {
            $values[] = $this->createRow($item);
        }

        return $values;
    }

    private function createHeader(): array
    {
        return [
            'EntityId',
            'CategoryName',
            'Sku',
            'Name',
            'Description',
            'ShortDescription',
            'Price',
            'Link',
            'Image',
            'Brand',
            'Rating',
            'CaffeineType',
            'Count',
            'Flavored',
            'Seasonal',
            'InStock',
            'Facebook',
            'KCup',
        ];
    }

    private function createRow(Coffee $item): array
    {
        return [
            $item->getEntityId(),
            $item->getCategoryName(),
            $item->getSku(),
            $item->getName(),
            $item->getDescription(),
            $item->getShortDescription(),
            $item->getPrice(),
            $item->getLink(),
            $item->getImage(),
            $item->getBrand(),
            $item->getRating(),
            $item->getCaffeineType(),
            $item->getCount(),
            $this->getStringRepresentationOfNullableBool($item->getFlavored()),
            $this->getStringRepresentationOfNullableBool($item->getSeasonal()),
            $this->getStringRepresentationOfNullableBool($item->isInStock()),
            $this->getStringRepresentationOfNullableBool($item->isFacebook()),
            $this->getStringRepresentationOfNullableBool($item->isKCup()),
        ];
    }

    private function getStringRepresentationOfNullableBool(?bool $value): string
    {
        if (true === $value) {
            return 'Yes';
        }

        if (false === $value) {
            return 'No';
        }

        return '';
    }
}
