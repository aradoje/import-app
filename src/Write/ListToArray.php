<?php

declare(strict_types=1);

namespace App\Write;

use App\CoffeeList;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
interface ListToArray
{
    public function createArrayWithHeader(CoffeeList $coffeeList): array;
}
