<?php

declare(strict_types=1);

namespace App;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class Coffee
{
    private int $entityId;
    private string $categoryName;
    private string $sku;
    private string $name;
    private string $description;
    private string $shortDescription;
    private string $price;
    private string $link;
    private string $image;
    private string $brand;
    private int $rating;
    private string $caffeineType;
    private ?int $count = null;
    private ?bool $flavored = null;
    private ?bool $seasonal = null;
    private bool $inStock;
    private bool $facebook;
    private bool $kCup;

    public function __construct(int $entityId)
    {
        $this->entityId = $entityId;
    }

    public function getEntityId(): int
    {
        return $this->entityId;
    }

    public function getCategoryName(): string
    {
        return $this->categoryName;
    }

    public function setCategoryName(string $categoryName): void
    {
        $this->categoryName = $categoryName;
    }

    public function getSku(): string
    {
        return $this->sku;
    }

    public function setSku(string $sku): void
    {
        $this->sku = $sku;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getShortDescription(): string
    {
        return $this->shortDescription;
    }

    public function setShortDescription(string $shortDescription): void
    {
        $this->shortDescription = $shortDescription;
    }

    public function getPrice(): string
    {
        return $this->price;
    }

    public function setPrice(string $price): void
    {
        $this->price = $price;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function setImage(string $image): void
    {
        $this->image = $image;
    }

    public function getBrand(): string
    {
        return $this->brand;
    }

    public function setBrand(string $brand): void
    {
        $this->brand = $brand;
    }

    public function getRating(): int
    {
        return $this->rating;
    }

    public function setRating(int $rating): void
    {
        $this->rating = $rating;
    }

    public function getCaffeineType(): string
    {
        return $this->caffeineType;
    }

    public function setCaffeineType(string $caffeineType): void
    {
        $this->caffeineType = $caffeineType;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function setCount(?int $count): void
    {
        $this->count = $count;
    }

    public function getFlavored(): ?bool
    {
        return $this->flavored;
    }

    public function setFlavored(bool $flavored): void
    {
        $this->flavored = $flavored;
    }

    public function getSeasonal(): ?bool
    {
        return $this->seasonal;
    }

    public function setSeasonal(bool $seasonal): void
    {
        $this->seasonal = $seasonal;
    }

    public function isInStock(): bool
    {
        return $this->inStock;
    }

    public function setInStock(bool $inStock): void
    {
        $this->inStock = $inStock;
    }

    public function isFacebook(): bool
    {
        return $this->facebook;
    }

    public function setFacebook(bool $facebook): void
    {
        $this->facebook = $facebook;
    }

    public function isKCup(): bool
    {
        return $this->kCup;
    }

    public function setKCup(bool $kCup): void
    {
        $this->kCup = $kCup;
    }
}
