<?php

declare(strict_types=1);

namespace App;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class GoogleSheetServiceFactory
{
    public function __invoke(): \Google_Service_Sheets
    {
        return new \Google_Service_Sheets(new \Google_Client());
    }
}
