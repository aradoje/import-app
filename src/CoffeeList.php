<?php

declare(strict_types=1);

namespace App;

use Assert\Assertion;

/**
 * @author Radoje Albijanic <radoje.albijanic@gmail.com>
 */
class CoffeeList
{
    private array $items;

    public function __construct(array $items = [])
    {
        Assertion::allIsInstanceOf($items, Coffee::class);
        $this->items = $items;
    }

    /**
     * @return Coffee[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    public function addItem(Coffee $coffee): void
    {
        $this->items[] = $coffee;
    }
}
